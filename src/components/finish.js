import React from 'react';
import Boxes from './playerBox';
import Bar from './comunes/barFinish';
import question from '../data/questions.js'

let i=0;
class Finish extends React.Component{
	constructor(props) {
    super(props);
	    this.state = {
	      isPlay: true,
	      team1Results: 0,
	      team2Results: 0,
	      team3Results: 0,
	      team4Results: 0,
	    };
  	};
	componentWillMount() {
		this.changeUtil();
 	}
  	changeUtil(){
  		/*console.log(question.questions[0].correct);*/
  			/*console.log(this.props.team1[0]);*/
  		question.questions.map(item=>{
  			/*team1.map(item=>{
  				console.log(item);
  			})*/
  			if (this.props.team1[i] === item.correct) { 
          this.setState(prevState => {
             return {team1Results: prevState.team1Results + 1}
          })
  			}
  			if (this.props.team2[i] === item.correct) { 
  				this.setState(prevState => {
             return {team2Results: prevState.team2Results + 1}
          })
  			}
  			if (this.props.team3[i] === item.correct) { 
  				this.setState(prevState => {
             return {team3Results: prevState.team3Results + 1}
          })
  			}
  			if (this.props.team4[i] === item.correct) { 
  				this.setState(prevState => {
             return {team4Results: prevState.team4Results + 1}
          })
  			}
  			i=i+1;
  		})
  	}
  	render(){
		return(
			<div>
				<Bar/>
        <div class="container-fluid center ">
          <center>
          	<Boxes 
    					players={this.props.players} 
    					isPlay={false}
              // Cambie esto para que cuadrara con los controles de evert
    					team1Results={this.state.team1Results}
    					team2Results={this.state.team2Results}
    					team3Results={this.state.team3Results}
    					team4Results={this.state.team4Results}
    				/>
    			</center>
        </div>

			</div>
		)		
  	}
  }
export default (Finish); 