import React from 'react';
import { Modal, TextInput, Button } from 'react-materialize';


class FormPlayers extends React.Component{
	constructor(props){
		super(props);
		this.state={
			name: "",
  		    players: [{ name: "" }]
		}
		    this.savePlayers = this.savePlayers.bind(this);

	}

	nameCHange = evt => {
	    this.setState({ name: evt.target.value });
	  };

	nameChangePlayer = idx => evt => {
		const newplayers = this.state.players.map((player, sidx) => {
	  if (idx !== sidx) return player;
	  return { ...player, name: evt.target.value };
		}
	);

	this.setState({ players: newplayers });
	};
	addPlayer = () => {
    this.setState({
      players: this.state.players.concat([{ name: "" }])
    });
  };
  removePlayer = idx => () => {
    this.setState({
      players: this.state.players.filter((s, sidx) => idx !== sidx)
    });
  };

/*  onChange(e){
         this.props.onChange(this.state.players);
  }*/
  savePlayers(e){
         this.props.savePlayers(this.state.players);
  };	
	render(){
		return(
			<div>
				<Modal 
				
					options={{
					dismissible: false,
					}}
					open={true} id="modal1" header="Configuración del juego">
					<form onSubmit={this.props.onChange}>
						{this.state.players.map((player, idx) => (
							<div className="row">
								<input
								style={{fontSize: '2.5em'}}
								className="col s10"
								type="text"
								placeholder={`Nombre del Equipo #${idx + 1}`}
								value={player.name}
								onChange={this.nameChangePlayer(idx)}
								/>
								<Button
								waves="light"
								onClick={this.removePlayer(idx)}
								className="red"
								>
								-
								</Button>
							</div>
						))}

					{this.state.players.length > 3 ? (
						<Button 
						disabled
						waves="light" onClick={this.addPlayer}
						style={{marginRight: '5px'}}>
						Añadir equipo
						</Button>
						):(
							<Button 
							waves="light" onClick={this.addPlayer}
							style={{marginRight: '5px'}}>
							Añadir equipo
							</Button>
						)}
						<Button 
							waves="light" onClick={this.savePlayers}
							style={{marginRight: '25px'}}>
							Continuar
							</Button>
						</form>
				</Modal>
			</div>	
		);
	}
}
export default FormPlayers;
