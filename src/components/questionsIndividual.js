import React from 'react'

export default (props)=>(
	<div>
		<ul className="collection with-header center blue-grey lighten-5">
			<li className="collection-header green darken-1 white-text"><h2>{props.question}</h2></li>
			{props.answers.map(item=>{
				let classColor="collection-item blue-grey lighten-5";
   				return(
					<h5 className="blue-grey lighten-5">
						<li className={classColor + " left-align"}>{item.option+" "+item.answer}</li>
					</h5>		
				)
       		})}
		</ul>
	</div>
)