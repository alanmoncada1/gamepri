import React from 'react'
import Questions from './questionsIndividual'
import Boxes from './playerBox'
import Indicator from './Indicator'
import Finish from './finish'
import FormPlayers from './formPlayers'
//import questions from '../data/questions.js'
import colors from './colors.js'
import PouchDB from 'pouchdb';


let key, counter=0; 
var team1ResultsAux=[]; 
var team2ResultsAux=[]; 
var team3ResultsAux=[]; 
var team4ResultsAux=[];
let player1Key = false;
let player2Key = false;
let player3Key = false;
let player4Key = false;
class question extends React.Component {
	constructor(props) {
		console.log(props.questions[0].doc);
    super(props);
    this.state = {
      isPlay: true,
      team1: '',
      team2: '',
      team3: '',
      team4: '',
      time: 30,
      aux: null,
      question: [props.questions[0].doc.question],
      answers: [props.questions[0].doc.answers],
      correct: [props.questions[0].doc.correct],
    };
  };
	componentDidMount() {
		
	    
 	}
 	onRevive(){
 		document.addEventListener("keydown", this.handleKeyPress, true);
		    this.timerID = setInterval(
		      () => this.count(),
		      1000
		    );
 	}
 	onChange = (nameTeams) => {
      this.setState({nameTeams: nameTeams});
      this.onRevive()
	}
 	handleKeyPress = (event) => {
 		// Anular doble pulsacion
 		if (event.keyCode != 0) {
 			 key = event.keyCode;
		}
 		if(player1Key){
 			if (key === 81
 				|| key === 87 ||
 				key === 69 ||
 				key === 82) {key = 0;}
 		}
 		if(player2Key){
 			if (key === 65
 				|| key === 83 ||
 				key === 68 ||
 				key === 70) {key = 0;}
 		}
 		if(player3Key){
 			if (key === 90
 				|| key === 88 ||
 				key === 67 ||
 				key === 86) {key = 0;}
 		}
 		if(player4Key){
 			if (key === 85
 				|| key === 73 ||
 				key === 79 ||
 				key === 80) {key = 0;}
 		}
	  switch(key){
	  	// Enter
	    case 13:
	    	this.setState({time: 0});
	    	/*console.log('A) Equipo 1');*/
	    	break;
	  	// Team 1
	    case 81:
	    	this.playGame('a',1);
	    	player1Key = true;
	    	/*console.log('A) Equipo 1');*/
	    	break;
		case 87:
	    	this.playGame('b',1);
	    	player1Key = true;
		    /*console.log('B) Equipo 1');*/
		    break;
		case 69:
	    	this.playGame('c',1);
	    	player1Key = true;
		    /*console.log('C) Equipo 1');*/
		    break;
		case 82:
	    	this.playGame('d',1);	
	    	player1Key = true;
		    /*console.log('D) Equipo 1');*/
		    break;
	    // Team 2
		  case 65:
	    	this.playGame('a',2);	
	    	player2Key = true;
	    	/*console.log('A) Equipo 2');*/
	    	break;
	      case 83:
	    	this.playGame('b',2);	
	    	player2Key = true;
		    /*console.log('B) Equipo 2');*/
		    break;
		  case 68:
	    	this.playGame('c',2);	
	    	player2Key = true;
		    /*console.log('C) Equipo 2');*/
		    break;
		  case 70:
	    	this.playGame('d',2);	
	    	player2Key = true;
		    /*console.log('D) Equipo 2');*/
		    break;
		  // Team 3
		  case 90:
	    	this.playGame('a',3);	
	    	player3Key = true;
	    	/*console.log('A) Equipo 3');*/
	    	break;
	      case 88:
	    	this.playGame('b',3);	
	    	player3Key = true;
		    /*console.log('B) Equipo 3');*/
		    break;
		  case 67:
	    	this.playGame('c',3);	
	    	player3Key = true;
		    /*console.log('C) Equipo 3');*/
		    break;
		  case 86:
	    	this.playGame('d',3);	
	    	player3Key = true;
		    /*console.log('D) Equipo 3');*/
		    break;
		  // Team 4
		  case 85:
	    	this.playGame('a',4);	
	    	player4Key = true;
	    	/*console.log('A) Equipo 4');*/
	    	break;
    	  case 73:
	    	this.playGame('b',4);	
	    	player4Key = true;
		    /*console.log('B) Equipo 4');*/
		    break;
		  case 79:
	    	this.playGame('c',4);	
	    	player4Key = true;
		    /*console.log('C) Equipo 4');*/
		    break;
		  case 80:
	    	this.playGame('d',4);	
	    	player4Key = true;
		    /*console.log('D) Equipo 4');*/
		    break;
 		return key;
	  };
	}
	playGame(option,player){
		switch(player){
			case 1:
				this.setState({
					team1: option,
					team1Results: option 
				});
				break;
			case 2:
				this.setState({
					team2: option, 
					team2Results: option 
				});
				break;
			case 3:
				this.setState({
					team3: option, 
					team3Results: option 
				});
				break;
			case 4:
				this.setState({
					team4: option, 
					team4Results: option 
				});
				break;
		}
	}
	count(){
		counter++;
		if (this.state.aux===this.props.questions.length) {
		    this.setState({
		    	isPlay: false,
		    	nameTeams: this.state.nameTeams,
		    	team1Results: team1ResultsAux,
		    	team2Results: team2ResultsAux,
		    	team3Results: team3ResultsAux,
		    	team4Results: team4ResultsAux
		    })
		}
		if(this.state.time==0){
			counter=0;
			team1ResultsAux.push(this.state.team1);
			team2ResultsAux.push(this.state.team2);
			team3ResultsAux.push(this.state.team3);
			team4ResultsAux.push(this.state.team4);
			player1Key = false;
			player2Key = false;
			player3Key = false;
			player4Key = false;
			this.setState({
				false: true,
				time: 30,
				aux: this.state.aux+1,
				team1: '',
				team2: '',
				team3: '',
				team4: '',

			});
			if (this.props.questions[this.state.aux]) {
				this.setState({
				question: this.props.questions[this.state.aux].doc.question,
				answers: this.props.questions[this.state.aux].doc.answers,
				correct: this.props.questions[this.state.aux].doc.correct,
			});
				/*console.log(questions.questions[this.state.aux].id+" "+this.state.aux);*/
			}
		}
		if (this.state.aux==null) {
			this.setState({
				aux:0,
				question: this.props.questions[0].doc.question,
				answers: this.props.questions[0].doc.answers,
				correct: this.props.questions[0].doc.correct,
			});
		}
		/*questions.questions[0].question*/
		this.setState({
			time: this.state.time-1,
			color: colors.colors[counter].color,
		});
	}

 	render() {
 		let view;
 		if (this.state.isPlay) {
 						if (this.state.nameTeams != null) {
						view	= <div>
								<Indicator 
									time={this.state.time}
									color={this.state.color}
								/>
								<Questions question={this.state.question} answers={this.state.answers}/>
								<Boxes players={this.state.nameTeams}
								isPlay={true}
								team1={ this.state.team1 }
								team2={ this.state.team2 }
								team3={ this.state.team3 }
								team4={ this.state.team4 }
								correct={ this.state.correct }
								/>
							</div>;					        
						}else{
							view = <FormPlayers savePlayers = {this.onChange}/>;
						}	
 					}
 					else{
 						view = <Finish 
 							players={this.state.nameTeams}
							team1={ this.state.team1Results }
							team2={ this.state.team2Results }
							team3={ this.state.team3Results }
							team4={ this.state.team4Results }/>;
					}
 		return(
 			<div>
 				{view}
   		 	</div>
 		)
 	}
}
export default (question);