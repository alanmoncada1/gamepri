import React from 'react';

export default (props)=>(
	<div>
		<a 
			value={props.value}
			onClick={props.onClick}
			data-position={props.dataPosition} 
			style={props.styleButton} 
			className={props.classButton}>
			<i style={props.styleIcon} className={props.classIcon}>{props.nameIcon}</i>
		</a>
	</div>		
)
