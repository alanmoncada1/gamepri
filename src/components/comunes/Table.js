import React from 'react'
import { questions } from '../../data/questions'
import Actions from '../Actions'
import M from "materialize-css";
import PouchDB from 'pouchdb';
import { RadioGroup } from 'react-materialize';

const questionsAux=[];
export default class Table extends React.Component {
	constructor(props) {
    	super(props);
    	this.state = {
      		idQuestion: null,
    	  	answers: {},
  			correctAnswer: null,
  			answer4: null,
  			answer3: null,
  			answer2: null,
  			answer1: null,
  			question: null,
  			correct: null,
  			isEdit: null,
  			idEdit: null,
		};
		this.editQuestion = this.editQuestion.bind(this);
  	}
	componentDidMount() {
		// Get questions
		this.getQuestions();
		const options = {
			onOpenStart: () => {
			console.log("Open Start");
			},
			onOpenEnd: () => {
			console.log("Open End");
			},
			onCloseStart: () => {
			console.log("Close Start");
			},
			onCloseEnd: () => {
			console.log("Close End");
			let instance = M.Modal.getInstance(this.Modal);
			instance.close();
			instance.destroy();
			window.location.reload(false);
			},
			opacity: 0.5,
			dismissible: false,
			startingTop: "4%",
			endingTop: "10%"
		};
		M.Modal.init(this.Modal, options);
		// If you want to work on instance of the Modal then you can use the below code snippet 
		// let instance = M.Modal.getInstance(this.Modal);
		// instance.close();
		// instance.open();
		// instance.destroy();
	}
	handleChange = (e) => {
		this.setState({correctAnswer: e.target.value});
	}
	// On submit modal
	onSubmit = (e) => { 
		e.preventDefault();
		var db = new PouchDB('questions');
		if (!this.state.isEdit) {
			// this.refs.question.value
			var questions = 
				{"_id": new Date().toISOString(),"question":this.refs.question.value,"image":"","answers":[
				{"answer":this.refs.answer1.value,"color":"teal darken-4",
				"option":"A)"},
				{"answer":this.refs.answer2.value,"color":"red darken-4",
				"option":"B)"},
				{"answer":this.refs.answer3.value,"color":"yellow darken-2",
				"option":"C)"},
				{"answer":this.refs.answer4.value,"color":"yellow darken-4",
				"option":"D)"}],
				"correct":this.state.correctAnswer};
			db.put(questions, function callback(err, result) {
		    if (!err) {
		      console.log('Successfully posted a questions!');
		    }
		    	console.log(err)
			});	
			db.allDocs({include_docs: true, descending: true}, function(err, doc) {
			    console.log(doc.rows);
			    // Comment That
			    window.location.reload(false);
			}); 
		}else{
			db.get(this.state.idEdit).then(function(doc) {
			  return db.put({
			    _id: doc._id,
			    _rev: doc._rev,
			    question: this.refs.question.value,
			    answers: [
					{"answer":this.refs.answer1.value,"color":"teal darken-4",
					"option":"A)"},
					{"answer":this.refs.answer2.value,"color":"red darken-4",
					"option":"B)"},
					{"answer":this.refs.answer3.value,"color":"yellow darken-2",
					"option":"C)"},
					{"answer":this.refs.answer4.value,"color":"yellow darken-4",
					"option":"D)"}],
				correct: this.state.correctAnswer
			  });
			}).then(function(response){
				console.log(response);
				this.setState({
					isEdit: false,
					idEdit: false,
				});
			    window.location.reload(false);
			}).catch(function(err){
				console.log(err);
			})
			
		}
	}
	destroy (question){
		var db = new PouchDB('questions');
		db.get(question).then(function(doc) {
		 	window.confirm("Seguro que quieres borrar esta pregunta?") ?
			   db.remove(doc).then(alert("Pregunta borrada"))
			:
			alert("No se ha borrado la pregunta");
		}).then(function (result) {
		  console.log("pregunta borrada");
		  window.location.reload(false);

		}).catch(function (err) {
		  console.log(err);
		});
	}
	editQuestion = async(question) =>	{
		var db = new PouchDB('questions');
		// Try to get question
		try {
			// Vars for docs pouchDB
			var result = await db.get(question);
			// Get the rows of array
		    this.setState({
		    	question: result.question,
		    	correct: result.correct,
		    	answer1: result.answers[0].answer,
		    	answer2: result.answers[1].answer,
		    	answer3: result.answers[2].answer,
		    	answer4: result.answers[3].answer,
		    	isEdit: true,
		    	idEdit: question,
		    });
			console.log(result);
		}catch (err){
			console.log(err);
		}
	}
	
	// Get data 
	getQuestions = async() => {
		var db = new PouchDB('questions');
		// Try to get question
		try {
			// Vars for docs pouchDB
			var result = await db.allDocs({
				include_docs: true,
				attachments: true
			});
			// Get the rows of array
		    let questions = result.rows;
	    	// Set questions to state
		    this.setState({
		      questions: questions.map((question, i) => (
		      	// Set the view of state
		        <li className="collection-item">
					<div className="row">
						<div className="col s5 truncate">
							{ question.doc.question }
						</div>
						<div className="col s2 center">
							{ question.doc.answers.length }
						</div>
						<div className="col s2 right">
							<Actions 
							editQuestion={ () => this.editQuestion(question.doc._id) }
							destroyQuestion={ () => this.destroy(question.doc._id) }
								/>
						</div>
					</div>
				</li>
		      ))
		    });
	 	} catch (err) {
	 		// Catch the error
	    	console.log(err);
		}
	}
	// Render HTML
	render(){
		const {
			questionValue,
			responseValue,
		} = this.state;
		return(
			<div>	
				<ul className="collection">
					<li className="collection-item">
						<div className="row">
							<div className="col s5 center">
								Texto de pregunta
							</div>
							<div className="col s2 center">
								Numero de respuestas
							</div>
							<div className="col s5 center">
								Acciones
							</div>
						</div>
					</li>	
					{ this.state.questions }
				</ul>
	        <div className="fixed-action-btn">
				<a onClick={this.newQuestion}
					data-target="modal1"
					className="btn-floating btn-large red darken-3 modal-trigger">
					<i className="large material-icons">add</i>
				</a>
			</div>
	        <div
	          ref={Modal => {
	            this.Modal = Modal;
	          }}
	          id="modal1"
	          className="modal"
	        >
			<form onSubmit = {this.onSubmit}>
	          <div className="modal-content">
	            <h4>Pregunta</h4>
				      <div className="row">
				        <div className="input-field col s12">
				          <textarea id="question" ref="question" type="text" 
				           placeholder={ this.state.question }
				          onChange={this.newQuestion}/>
				          <label for="question">Texto de pregunta</label>
				        </div>

					        <div className="input-field col s12">
					          <input id="answer1" ref="answer1" type="text" placeholder={ this.state.answer1 }/>
					          <label for="answer1">Respuesta 1</label>
					        </div>
					        <div className="input-field col s12">
					          <input id="answer2" ref="answer2" type="text" placeholder={ this.state.answer2 }/>
					          <label for="answer2">Respuesta 2</label>
					        </div>
					        <div className="input-field col s12">
					          <input id="answer3" ref="answer3" type="text" placeholder={ this.state.answer3 }/>
					          <label for="answer3">Respuesta 3</label>
					        </div>

					        <div className="input-field col s12">
					          <input id="answer4" ref="answer4" type="text" placeholder={ this.state.answer4 }/>
					          <label for="answer4">Respuesta 4</label>
					        </div>
					        { this.state.isEdit ? 
					        <div className="row">
					        	<h5>Respuesta Correcta</h5>
							    <p className="col s3">
							      <label>
							        <input className="with-gap" name="group1" type="radio" value="a" 
							        	// checked={ this.state.correct =='a' ? true : false }
							        onClick={this.handleChange} />
							        <span>Respuesta 1</span>
							      </label>
							    </p>
							    <p className="col s3">
							      <label>
							        <input className="with-gap" name="group1" type="radio" value="b" 
							        	// checked={ this.state.correct =='b' ? true : false }
							        onClick={this.handleChange}  />
							        <span>Respuesta 2</span>
							      </label>
							    </p>
							    <p className="col s3">
							      <label>
							        <input className="with-gap" name="group1" type="radio" value="c" 
							        	// checked={ this.state.correct =='c' ? true : false }
							        onClick={this.handleChange} />
							        <span>Respuesta 3</span>
							      </label>
							    </p>
							    <p className="col s3">
							      <label>
							        <input className="with-gap" name="group1" type="radio" value="d" 
							        	// checked={ this.state.correct =='d' ? true : false }
						        	onClick={this.handleChange} />
							        <span>Respuesta 4</span>
							      </label>
							    </p>
				     		</div>
				     		: 
				     	<div className="row">
					        	<h5>Respuesta Correcta</h5>
							    <p className="col s3">
							      <label>
							        <input className="with-gap" name="group1" type="radio" value="a" 
							        	// checked
							        onClick={this.handleChange} />
							        <span>Respuesta 1</span>
							      </label>
							    </p>
							    <p className="col s3">
							      <label>
							        <input className="with-gap" name="group1" type="radio" value="b" 
							        	
							        onClick={this.handleChange}  />
							        <span>Respuesta 2</span>
							      </label>
							    </p>
							    <p className="col s3">
							      <label>
							        <input className="with-gap" name="group1" type="radio" value="c" 
							        	
							        onClick={this.handleChange} />
							        <span>Respuesta 3</span>
							      </label>
							    </p>
							    <p className="col s3">
							      <label>
							        <input className="with-gap" name="group1" type="radio" value="d" 
							        	
						        	onClick={this.handleChange} />
							        <span>Respuesta 4</span>
							      </label>
							    </p>
				     		</div>}
				      </div>
				</div>
	          <div className="modal-footer">
	            <a className="modal-close waves-effect waves-red btn-flat">
	              Cancelar
	            </a>
	            <button type = 'submit' className="waves-effect waves-green btn-flat" value='Guardar'>Submit</button>
	          </div>
	  		</form>
        	</div>
		</div>
			)
	}
}