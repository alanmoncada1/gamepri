import React from 'react'

class Bar extends React.Component	{
	handleClick(){
		window.location.replace('');
	};
	render(){
		return(
			<div>	
				<nav className="red darken-3">
					<div className="nav-wrapper">
					  <a href="#" className="brand-logo center">Resultados</a>
					  <a onClick={this.handleClick} className="brand-logo left"><i className="material-icons right">home</i></a>
					</div>
				</nav>   
			</div>
		);
	}
}
export default (Bar);
