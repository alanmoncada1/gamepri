import React from 'react'
import { Button } from 'react-materialize';
import Icon from './Icon'

class Indicator extends React.Component	{
	constructor(props) {
	    super(props);
		/*this.state = {
	      count: true,
	    };*/
	}
	render(){
		return(
			<div>
				<Button

				style={{
					marginLeft: "90%",
					fontSize: "2em",
					width: "110px",
					height: "110px"}}
				  floating
				  large
				  className={this.props.color}
				  icon={this.props.time}
				/>
			</div>
		);
	}
}

export default (Indicator);