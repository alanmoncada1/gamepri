const colors=[
{"color":"green darken-4 pulse"},
{"color":"green darken-3 pulse"},
{"color":"green darken-2 pulse"},
{"color":"green darken-1 pulse"},
{"color":"green pulse"},
{"color":"green pulse"},
{"color":"light-green lighten-1 pulse"},
{"color":"light-green lighten-2 pulse"},
{"color":"light-green lighten-3 pulse"},
{"color":"light-green black-text lighten-4 pulse"},
{"color":"light-green black-text lighten-5 pulse"},

{"color":"yellow accent-4 black-text pulse"},
{"color":"yellow accent-4 black-text pulse"},
{"color":"yellow accent-3 pulse"},
{"color":"yellow accent-2 pulse"},
{"color":"yellow accent-1 pulse"},
{"color":"yellow pulse"},
{"color":"yellow pulse"},
{"color":"yellow accent-1 pulse"},
{"color":"yellow accent-2 pulse"},
{"color":"yellow accent-3 pulse"},
{"color":"yellow accent-4 pulse"},
{"color":"yellow accent-4 pulse"},



{"color":"#ffebee red lighten-5 black-text pulse"},
{"color":"#ffebee red lighten-4 black-text pulse"},
{"color":"#ffebee red lighten-3 pulse"},
{"color":"#ffebee red lighten-3 pulse"},
{"color":"#ffebee red lighten-3 pulse"},
{"color":"#ffebee red lighten-2 pulse"},
{"color":"#ffebee red lighten-1 pulse"},
{"color":"#ffebee red pulse"},
{"color":"#ffebee red pulse"}
];

module.exports = {
  colors
};
