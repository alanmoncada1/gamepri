import React from 'react';
import ButtonSlider from './comunes/buttonSlider'
import '../css/styles.css'
import { HashRouter, router } from "react-router-dom";
import { Slider } from 'react-materialize';

let settingsButton={ 
	marginRight: "100%"
};
let bigButton={
	marginTop: "13%", 
	width: "200px",
	height: "200px"
};
let sizeOfIcon={
	fontSize: "10em", 
	marginTop: "35%"
}
class Slide extends React.Component {
	constructor(props) {
    	super(props);
    	this.handleClick = this.handleClick.bind(this);
	};
	handleClick() {
    window.location.hash ='settings';
	};

  	render() {
  		return(
				<Slider className="fullscreen" options={{indicators: false}}>
					<ul className="slides">
						<li>
							<img src="1.png"/> 
							<div className="caption center-align">
							<ButtonSlider 
								onClick={this.handleClick}
								styleButton={ settingsButton }
								dataPosition="left" 
								classButton="btn-floating teal pulse"
								classIcon="material-icons"
								nameIcon="build"/>
							<div className="container grey darken-3">
								<h3>¿Podrás con el reto?</h3>
								<h5 className="light grey-text text-lighten-3">Responde las preguntas correctamente.</h5>
							</div>
									<ButtonSlider 
										onClick={this.props.onChange}
										styleButton={bigButton}
										dataPosition="right" 
										classButton="btn-floating btn-large red darken-3 pulse"
										classIcon="material-icons"
										styleIcon={sizeOfIcon}
										nameIcon="play_arrow"/>
							</div>
						</li>
					</ul>
				</Slider>
  			);
  	}
}
export default Slide;