import React from 'react';
import question from '../data/questions.js'
import MdClose from 'react-ionicons/lib/MdClose'
import MdCheckmark from 'react-ionicons/lib/MdCheckmark'
import IosTimeOutline from 'react-ionicons/lib/IosTimeOutline'
import { Chart } from "react-google-charts";


let color, max=0, champion;
class Card extends React.Component{
	constructor(props){
		super(props);
		this.state={
		}
	}
	componentDidMount(){
	}
	render(){
			
		const options = {
			title: "Resultados ",
			chartArea: { width: '50%' },
			colors: [this.props.colorChart, '#fff'],
		};
	   	let correct=this.props.isCorrect;
	   	let aux;
	   	if (this.props.isPlay) {

	   	aux =	<div className={this.props.color}>
			   
			    <div className="card-content">
			      <span className="black-text"><h5>{this.props.cont}</h5></span>
			      <h5 className="center">
				   {
				   	(function(){
			   			switch(correct){
			   				case '':
		   					case 'true':
	   						case 'false':
	   							
			   			}
				   })()}   
			      </h5>
			  	</div>
				  </div>;
	   	}else{
	   		   	aux =
					 	<div className="row">
					 		<div className="card z-depth-3 col s11">
					 			{/*Type the name*/}
						 		<h5 className="black-text">{this.props.cont}</h5>
						 		<div className="row">
							 		<h5 className="col s6 black-text">Aciertos: {this.props.results}</h5>
						  		 	{this.props.champion ? <img className="col s6" width="10px" height="150px" src="./trofeo.png"/> :
							   		<div style={{marginTop: '30%'}}></div>}
						 		</div>
						 	     <Chart
								      chartType="Bar"
								      data={
									    [
										    ['equipo', 'aciertos', 'total'],
											[this.props.cont, this.props.results, 16],

									  	]}
								      options={options}
								      width="100%"
								      height="300px"
								    />
					 		</div>
						</div>;
	   	}
		return(
			<div className="col s3 m3 ">
			  {aux}
			</div>
		);
	}	
}

export default Card;