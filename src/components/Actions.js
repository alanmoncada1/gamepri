import React from 'react';

export default (props)=>(
	<div>
		{/*<a onClick={props.editQuestion.bind(this)} data-target="modal1" className="waves-effect waves-light btn teal modal-trigger"><i className="material-icons">create</i></a>*/}
		<a onClick={props.destroyQuestion.bind(this)} className="waves-effect waves-light btn red darken-3"><i className="material-icons">delete</i></a>
	</div>		
)
