import React from 'react'
import '../css/styles.css'
import Card from './cards.js'

let max=0, value;
class cards extends React.Component	 {
	constructor(props) {
    super(props);
    this.state = {
      isPlay: true,
      team1Results: '',
      team2Results: '',
      team3Results: '',
      team4Results: '',
    };
  };


	render() {
		console.log(this.props.team1Results);
		console.log(this.props.team2Results);
		console.log(this.props.team3Results);
		console.log(this.props.team4Results);
		const cards = []
		var props = this.props;
		let aux='';


		for (let i = 0; i < this.props.players.length; i++) {
		  cards.push(
		  	<div>
		  		{(function() {
	
					        switch(i) {
								case 0:
									let color='card horizontal ';
									let isCorrect='';
									if (!props.isPlay) {
										if (max<props.team1Results) {
											max=props.team1Results;
										}
										if (props.team1Results === max && props.team1Results != 0) {
											value=true;
										}else{
											value=false;
										}
											aux = <Card 
												exist1={true}
												isPlay={false}
												cont={props.players[i].name}
												contTeam1={props.players[i].name}
												results={props.team1Results}
												resultsTeam1={props.team1Results}
												champion={value}
												color='card horizontal '
												colorChart='#6A3787'
												/>;
									}else{
										// Selection of color
										if (props.team1 != '') {
											if (props.team1 === props.correct) {
												color=color+"green lighten-1";
												isCorrect='true';
											}else{
												color=color+"red lighten-1"
												isCorrect='false';
											}
										}
										aux = <Card 
											isPlay={true}
											cont={props.players[i].name}
											isCorrect={isCorrect}
											color={color}
											/>;
									}
									return aux;
								case 1:
									let color2='card horizontal ';
									let isCorrect2='';
									if (!props.isPlay) {
										if (max<props.team2Results) {
											max=props.team2Results;
										}
										if (props.team2Results === max && props.team2Results != 0) {
											value=true;
										}else{
											value=false;
										}
											aux = <Card 
												exist2={true}
												isPlay={false}
												cont={props.players[i].name}
												contTeam2={props.players[i].name}
												results={props.team2Results}
												resultsTeam2={props.team2Results}
												champion={value}
												colorChart='#188ABA'
												color='card horizontal '
												/>;
									}else{
										// Selection of color2
										if (props.team2 != '') {
											if (props.team2 === props.correct) {
												color2=color2+"green lighten-1";
												isCorrect2='true';
											}else{
												color2=color2+"red lighten-1"
												isCorrect2='false';
											}
										}
										aux = <Card 
											isPlay={true}
											cont={props.players[i].name}
											isCorrect={isCorrect2}
											color={color2}
											/>;
									}
									return aux;

								case 2:
									let color3='card horizontal ';
									let isCorrect3='';
									if (!props.isPlay) {
										if (max<props.team3Results) {
											max=props.team3Results;
										}
										if (props.team3Results === max && props.team3Results != 0) {
											value=true;
										}else{
											value=false;
										}
											aux = <Card 
												exist3={true}
												isPlay={false}
												cont={props.players[i].name}
												contTeam3={props.players[i].name}
												results={props.team3Results}
												resultsTeam3={props.team3Results}
												colorChart='#CC6438'
												champion={value}
												color='card horizontal '
												/>;
									}else{
										// Selection of color
										if (props.team3 != '') {
											if (props.team3 === props.correct) {
												color3=color3+"green lighten-1";
												isCorrect3='true';
											}else{
												color3=color3+"red lighten-1"
												isCorrect3='false';
											}
										}
										aux = <Card 
											isPlay={true}
											cont={props.players[i].name}
											isCorrect={isCorrect3}
											color={color3}
											/>;
									}
									return aux;
								case 3:
									let color4='card horizontal ';
									let isCorrect4='';
									if (!props.isPlay) {
										if (max<props.team4Results) {
											max=props.team4Results;
										}
										if (props.team4Results === max && props.team4Results != 0) {
											value=true;
										}else{
											value=false;
										}
											aux = <Card 
												isPlay={false}
												exist4={true}
												cont={props.players[i].name}
												contTeam4={props.players[i].name}
												results={props.team4Results}
												resultsTeam4={props.team4Results}
												colorChart='#BF2256'
												champion={value}
												color='card horizontal '
												/>;
									}else{
										// Selection of color
										if (props.team4 != '') {
											if (props.team4 === props.correct) {
												color4=color4+"green lighten-1";
												isCorrect4='true';
											}else{
												color4=color4+"red lighten-1"
												isCorrect4='false';
											}
										}
										aux = <Card 
											isPlay={true}
											cont={props.players[i].name}
											isCorrect={isCorrect4}
											color={color4}
											/>;
									}
									return aux;
								default:
									return "ERROR";
				        	}
			      		}
			      	)()
		  		}
			 	
			 	</div>
		 	)
		 }

		return(
			<div className="">
				<div className="row">
					
					{cards}
					
				</div>
			</div>
		);
	}
}

export default (cards);