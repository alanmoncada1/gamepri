import React from 'react';

export default (props)=>(
	<i style={props.styleIcon} className="material-icon">{props.nameIcon}</i>
);
