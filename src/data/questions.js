const questions = [
{"id":1,"question":"¿Qué significa PRI?","image":"","answers":[
{"answer":"Partido Revolucionario Institucional","color":"teal darken-4",
"option":"A)"},
{"answer":"Partido Revolucionario Industrial","color":"red darken-4",
"option":"B)"},
{"answer":"Partido Real Industrial","color":"yellow darken-2",
"option":"C)"},
{"answer":"Partido Real Institucional","color":"yellow darken-4",
"option":"D)"}],
"correct":"a"},
{"id":2,"question":"¿Cuál es el slogan del PRI?","image":"","answers":[
{"answer":"Justicia y lealtad","color":"teal darken-4",
"option":"A)"},
{"answer":"Democracia y justicia social","color":"red darken-4",
"option":"B)"},
{"answer":"Justicia Nacionalista","color":"yellow darken-2",
"option":"C)"},
{"answer":"Democracia justa y con principios","color":"yellow darken-4",
"option":"D)"}],
"correct":"b"},
{"id":3,"question":"¿Cuales son los sectores del PRI?","image":"","answers":[
{"answer":"Coordial, Democratico, Prensa","color":"teal darken-4",
"option":"A)"},
{"answer":"Obrero, Industrial, Constitucional","color":"red darken-4",
"option":"B)"},
{"answer":"Agrario, Obrero y Popular","color":"yellow darken-2",
"option":"C)"},
{"answer":"Democrático, Electoral, Popular","color":"yellow darken-4",
"option":"D)"}],
"correct":"c"},
{"id":4,"question":"¿Cuál es el nombre del Instituto Reyes Heroles?","image":"","answers":[
{"answer":"Instituto Jesús Reyes Heroles (IRH)","color":"teal darken-4",
"option":"A)"},
{"answer":"Instituto Juan Reyes Heroles (IRH)","color":"red darken-4",
"option":"B)"},
{"answer":"Instituto José Reyes Heroles (IRH)","color":"yellow darken-2",
"option":"C)"},
{"answer":"Instituto Andrés Reyes Heroles (IARH)","color":"yellow darken-4",
"option":"D)"}],
"correct":"a"},
{"id":5,"question":"¿Qué instituciones ha creado el PRI?","image":"","answers":[
{"answer":"CONASUPO, NASA, Shell","color":"teal darken-4",
"option":"A)"},
{"answer":"IPN, ISSSTE, INFONAVIT, IFE, IMSS, CFE, PEMEX, INAH, libros texto gratuitos","color":"red darken-4",
"option":"B)"},
{"answer":"ONU, BANAMEX, Empresas privadas.","color":"yellow darken-2",
"option":"C)"},
{"answer":"Seguro Popular, Becas alimenticias, Plan de vacunación","color":"yellow darken-4",
"option":"D)"}],
"correct":"b"},
{"id":6,"question":"Nombra al presidente actual del Comité Directivo Estatal de Hidalgo","image":"","answers":[
{"answer":"Mtra. Erika Hernández Rodríguez","color":"teal darken-4",
"option":"A)"},
{"answer":"Mtro. Julio Valera Piedras","color":"red darken-4",
"option":"B)"},
{"answer":"Lic. Carolina Viggiano Austria","color":"yellow darken-2",
"option":"C)"},
{"answer":"Mtra. Erika Rodríguez Hernández","color":"yellow darken-4",
"option":"D)"}],
"correct":"d"},
{"id":7,"question":"¿En qué fecha se fundó el partido PNR?","image":"","answers":[
{"answer":"5 de octubre de 1929","color":"teal darken-4",
"option":"A)"},
{"answer":"21 de julio de 1933","color":"red darken-4",
"option":"B)"},
{"answer":"13 de noviembre de 1933","color":"yellow darken-2",
"option":"C)"},
{"answer":"4 de marzo de 1929","color":"yellow darken-4",
"option":"D)"}],
"correct":"d"},
{"id":8,"question":"¿Qué significa PNR?","image":"","answers":[
{"answer":"Partido Nacionalista Revolucionario","color":"teal darken-4",
"option":"A)"},
{"answer":"Partido Nueva Revolución","color":"red darken-4",
"option":"B)"},
{"answer":"Partido Nacional Revolucionario","color":"yellow darken-2",
"option":"C)"},
{"answer":"Partido de la Nueva Realidad","color":"yellow darken-4",
"option":"D)"}],
"correct":"c"},
{"id":9,"question":"¿Cuántos diputados tiene el PRI en la LXIV Legislatura?","image":"","answers":[
{"answer":"5","color":"teal darken-4",
"option":"A)"},
{"answer":"10","color":"red darken-4",
"option":"B)"},
{"answer":"12","color":"yellow darken-2",
"option":"C)"},
{"answer":"17","color":"yellow darken-4",
"option":"D)"}],
"correct":"a"},
{"id":10,"question":"¿Qué grupo legislativo fue el más eficiente en el primer año de trabajo legislativo de la LXIV Legislatura?","image":"","answers":[
{"answer":"Movimiento de Regeneración Nacional","color":"teal darken-4",
"option":"A)"},
{"answer":"Partido Acción Nacional","color":"red darken-4",
"option":"B)"},
{"answer":"Partido Nueva Alianza","color":"yellow darken-2",
"option":"C)"},
{"answer":"Partido Revolucionario Institucional","color":"yellow darken-4",
"option":"D)"}],
"correct":"d"},
{"id":11,"question":"¿Quién es el diputado con mayor eficiencia legislativa del primer año de la LXIV Legislatura?","image":"","answers":[
{"answer":"Dip. Juan Hernández","color":"teal darken-4",
"option":"A)"},
{"answer":"Dip. Mayka Ortega Eguiluz","color":"red darken-4",
"option":"B)"},
{"answer":"Dip. Julio Valera Piedras","color":"yellow darken-2",
"option":"C)"},
{"answer":"Dip. María Luisa Pérez Perusquía","color":"yellow darken-4",
"option":"D)"}],
"correct":"c"},
{"id":12,"question":"Menciona a los dos diputados del PRI que nunca faltaron a sesión  en el primer año de la LXIV Legislatura","image":"","answers":[
{"answer":"Lic. José Luis Espinosa y Lic. María Luisa Pérez","color":"teal darken-4",
"option":"A)"},
{"answer":"Lic. Adela Pérez Y Mtro. Julio Valera","color":"red darken-4",
"option":"B)"},
{"answer":"Lic. Mayka Ortega y Mtro. Julio Valera","color":"yellow darken-2",
"option":"C)"},
{"answer":"Lic. Mayka Ortega y Lic. José Luis Espinosa","color":"yellow darken-4",
"option":"D)"}],
"correct":"c"},
{"id":13,"question":"¿Cuales son los 5 ejes de trabajo del grupo legislativo del PRI en la LXIV Legislatura?","image":"","answers":[
{"answer":"Desarrollo social; economía; seguridad, justicia y paz social","color":"teal darken-4",
"option":"A)"},
{"answer":"Mujeres, salud, juventud, adultos mayores y madres solteras","color":"red darken-4",
"option":"B)"},
{"answer":"Economía, tecnología, arte, protección de animales y medio ambiente","color":"yellow darken-2",
"option":"C)"},
{"answer":"Educación, diplomacia, deportes, cultura y desarrollo social","color":"yellow darken-4",
"option":"D)"}],
"correct":"a"},
{"id":14,"question":"¿Quién es el diputado priista que emanó de una candidatura indígena para la LXIV Legislatura?","image":"","answers":[
{"answer":"Lic. José Luis Espinosa","color":"teal darken-4",
"option":"A)"},
{"answer":"Lic. Adela Pérez","color":"red darken-4",
"option":"B)"},
{"answer":"Mtra. Erika Rodríguez","color":"yellow darken-2",
"option":"C)"},
{"answer":"Lic. Mayka Ortega","color":"yellow darken-4",
"option":"D)"}],
"correct":"b"},
{"id":15,"question":"¿Por cuántos distritos locales está conformado el estado?","image":"","answers":[
{"answer":"7 distritos locales","color":"teal darken-4",
"option":"A)"},
{"answer":"20 distritos locales","color":"red darken-4",
"option":"B)"},
{"answer":"18 distritos locales","color":"yellow darken-2",
"option":"C)"},
{"answer":"17 distritos locales","color":"yellow darken-4",
"option":"D)"}],
"correct":"c"},

]

/*
























{"id":16,"question":"¿Cuál es el nombre científico del hombre?","image":"","answers":[
{"answer":"Homo erectus","color":"teal darken-4",
"option":"A)"},
{"answer":"Homo neanderthalensis","color":"red darken-4",
"option":"B)"},
{"answer":"Homo cromagnon","color":"yellow darken-2",
"option":"C)"},
{"answer":"Homo sapiens","color":"yellow darken-4",
"option":"D)"}],
"correct":"d"},
{"id":13,"question":"¿Qué civilización se considera la primera del mundo?","image":"","answers":[
{"answer":"Civilización egipcia.","color":"teal darken-4",
"option":"A)"},
{"answer":"Civilización mesopotámica.","color":"red darken-4",
"option":"B)"},
{"answer":"Civilización sumeria.","color":"yellow darken-2",
"option":"C)"},
{"answer":"Civilización china.","color":"yellow darken-4",
"option":"D)"}],
"correct":"c"},
{"id":17,"question":"¿Quién inventó la tabla periódica?","image":"","answers":[
{"answer":"Dmitri Mendeleev.","color":"teal darken-4",
"option":"A)"},
{"answer":"Galileo Galilei","color":"red darken-4",
"option":"B)"},
{"answer":"Leonardo DaVinci","color":"yellow darken-2",
"option":"C)"},
{"answer":"Jhon Dalton","color":"yellow darken-4",
"option":"D)"}],
"correct":"a"},
{"id":18,"question":"¿Cuál es el animal terrestre más rápido?","image":"","answers":[
{"answer":"La cucaracha","color":"teal darken-4",
"option":"A)"},
{"answer":"La liebre","color":"red darken-4",
"option":"B)"},
{"answer":"El jaguar","color":"yellow darken-2",
"option":"C)"},
{"answer":"El guepardo","color":"yellow darken-4",
"option":"D)"}],
"correct":"d"},
{"id":19,"question":"¿Cuánto tiempo duró el periodo bélico conocido como la Guerra de los 100 años?","image":"","answers":[
{"answer":"100 años","color":"teal darken-4",
"option":"A)"},
{"answer":"98 años","color":"red darken-4",
"option":"B)"},
{"answer":"116 años","color":"yellow darken-2",
"option":"C)"},
{"answer":"110 años","color":"yellow darken-4",
"option":"D)"}],
"correct":"a"},
{"id":20,"question":"¿Cuál es la causa principal del calentamiento global?","image":"","answers":[
{"answer":"Contaminación","color":"teal darken-4",
"option":"A)"},
{"answer":"Sobrepoblación","color":"red darken-4",
"option":"B)"},
{"answer":"El efecto invernadero.","color":"yellow darken-2",
"option":"C)"},
{"answer":"Cambio Climático","color":"yellow darken-4",
"option":"D)"}],
"correct":"a"}




¿en que fecha se fundó el partido?
¿Principios ideologicos?
*/
module.exports = {
  questions
}
