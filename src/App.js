import React from 'react';
import Slider from './components/slide';
import { questions } from './data/questions'
import Questions from './components/questions';
import Settings from './components/settings';
import logo from './logo.svg';
import './App.css';
import PouchDB from 'pouchdb';



class App extends React.Component {

  state={
    isPlay: false,
    isSettings: false
  }
  componentDidMount() {
    this.getQuestions();
  }
  // Get data 
  getQuestions = async() => {
    var db = new PouchDB('questions');
    // Try to get question
    try {
      // Vars for docs pouchDB
      var result = await db.allDocs({
        include_docs: true,
        attachments: true
      });
      // Get the rows of array
        let questions = result.rows;
        this.setState({
          questions: questions
        });
     } catch (err) {
       // Catch the error
        console.log(err);
    }
  }
    render(){
      return(  
        this.state.isPlay ? <Questions question={questions} questions={this.state.questions}/> : 
        <Slider onChange={() => 
          this.setState({isPlay: true})
        }/>
      ) 
    }
  }


export default App;
