import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter, Route } from "react-router-dom";
import Settings from './components/settings';
import Questions from './components/questions';
import Finish from './components/finish';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
    <HashRouter>
      <div>
        <Route path="/" exact     component={ App } />
        <Route path="/home" exact     component={ App } />
        <Route path="/settings/home" exact     component={ App } />
        <Route path="/settings"  component={ Settings } />
        <Route path="/questions"  component={ Questions } />
        <Route path="/finish"  component={ Finish } />
      </div>
    </HashRouter>, 
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
